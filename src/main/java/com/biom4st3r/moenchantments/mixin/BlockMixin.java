package com.biom4st3r.moenchantments.mixin;

import java.util.List;
import java.util.Optional;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.Logic.onBreakMixinLogic;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.BasicInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.SmeltingRecipe;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.loot.LootSupplier;
import net.minecraft.world.loot.LootTables;
import net.minecraft.world.loot.context.LootContext;
import net.minecraft.world.loot.context.LootContextParameters;
import net.minecraft.world.loot.context.LootContextTypes;

@Mixin(Block.class)
public class BlockMixin
{
    @Inject(at = @At("HEAD"),method = "onBreak",cancellable = false)
    public void afterBreak(World world, BlockPos blockPos, BlockState blockState, PlayerEntity pe,CallbackInfo ci)
    {
        // if(!MoEnchantmentsMod.isWhiteListPopulated())
        // {
        //     MoEnchantmentsMod.whitelistToBlock();
        // }
        onBreakMixinLogic.tryVeinMining(world, blockPos, blockState, pe);   
    }

    boolean lock = false;

    @Inject(at = @At("HEAD"),method = "getDroppedStacks", cancellable = true)
    public void doAlphaFire(BlockState blockState, net.minecraft.world.loot.context.LootContext.Builder lootBuilder, CallbackInfoReturnable<List<ItemStack>> ci)
    {

        ItemStack tool = lootBuilder.get(LootContextParameters.TOOL);
        Identifier loottableId = ((Block)(Object)this).getDropTableId();
        if(EnchantmentHelper.getLevel(MoEnchants.AUTOSMELT, tool) > 0 && loottableId != LootTables.EMPTY)
        {
            Entity player = lootBuilder.get(LootContextParameters.THIS_ENTITY);
            lootBuilder.put(LootContextParameters.BLOCK_STATE, blockState);
            LootContext lootContext = lootBuilder.build(LootContextTypes.BLOCK);
            ServerWorld serverworld = lootContext.getWorld();
            LootSupplier lootSuppier = serverworld.getServer().getLootManager().getSupplier(loottableId);
            List<ItemStack> stacks = lootSuppier.getDrops(lootContext);
            Optional<SmeltingRecipe> smeltingResult;
            RecipeManager rm = player.world.getRecipeManager();
            Inventory basicInv = new BasicInventory();
            ItemStack itemToBeChecked = ItemStack.EMPTY;
            for(int i = 0; i < stacks.size(); i++)
            {
                itemToBeChecked = stacks.get(i);
                basicInv = new BasicInventory(itemToBeChecked);
                smeltingResult = rm.getFirstMatch(RecipeType.SMELTING, basicInv, player.world);
                if(smeltingResult.isPresent() && !(MoEnchantmentsMod.autoSmelt_blacklist.contains(itemToBeChecked.getItem())))
                {
                    // CompoundTag itemTag = tool.getTag();

                    stacks.set(i, new ItemStack(smeltingResult.get().getOutput().getItem(),itemToBeChecked.getCount()));
                    // itemTag.putDouble("smeltexperience", ((smeltingResult.get().getExperience() * itemToBeChecked.getCount()) + 
                    //     (itemTag.containsKey("smeltexperience") ? itemTag.getDouble("smeltexperience") : 0)));

                    ((PlayerExperienceStore)(Object)player).addExp(smeltingResult.get().getExperience() * itemToBeChecked.getCount());
                    //tool.setTag(itemTag);
                    if(!lock)
                    {
                        lock = !lock;
                        serverworld.playSound((PlayerEntity)null, player.getBlockPos(), SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.1f, 0.1f);
                    }
                }
                
            }
            lock = false;
            ci.setReturnValue(stacks);
        }

    }
}