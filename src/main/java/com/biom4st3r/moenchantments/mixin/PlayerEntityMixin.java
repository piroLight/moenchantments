package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.biow0rks.Biow0rks;
import com.biom4st3r.biow0rks.Mxn;
import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.Enchantments.MoEnchant;
import com.biom4st3r.moenchantments.Logic.EnderProtectionLogic;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;
import com.biom4st3r.moenchantments.interfaces.PotionEffectRetainer;
import com.biom4st3r.moenchantments.interfaces.PotionRetentionTarget;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity implements PlayerExperienceStore {
    public PlayerEntityMixin(EntityType<? extends LivingEntity> entityType_1, World world_1) {
        super(entityType_1, world_1);
    }

    @Unique public float experienceStorage = 0;

    @Override
    public int getAndRemoveStoredExp() {
        if(experienceStorage >= 1f)
        {
            int experienceStorage_int = (int)experienceStorage;
            experienceStorage = experienceStorage-experienceStorage_int;
            Biow0rks.debug("Claimed %s/%s exp", experienceStorage_int, experienceStorage);
            return experienceStorage_int;
        }
        return 0; 
    }
    
    @Override
    public void addExp(float i) {
        Biow0rks.debug("storing %s exp", i);
        this.experienceStorage += i;
    }
    
    @Inject(at = @At(Mxn.At.MethodHead), method = "applyDamage", cancellable = true)
    public void enderProtectionImpl(DamageSource damageSource, float damage, CallbackInfo ci) {
        LivingEntity defender = (LivingEntity) (Object) this;
        int EnderProtectionLevel = EnchantmentHelper.getEquipmentLevel(MoEnchants.ENDERPROTECTION, defender);
        if (EnderProtectionLevel > 0) {
            if (EnderProtectionLogic.doLogic(damageSource, EnderProtectionLevel, defender)) 
            {
                ci.cancel();
            }
        }
    }

    @Inject(at = @At(Mxn.At.MethodHead),method = "attack")
    public void usePotionEffectFromImbued(Entity defender,CallbackInfo ci)
    {
        PotionEffectRetainer stack = ((PotionEffectRetainer) (Object) this.getMainHandStack());
        if(MoEnchants.POTIONRETENTION.isAcceptableItem(this.getMainHandStack()) && MoEnchant.hasEnchant(MoEnchants.POTIONRETENTION, this.getMainHandStack()) && stack.getCharges() > 0)
        {
            StatusEffectInstance effect = stack.useEffect();
            if(defender instanceof LivingEntity)
            {
                
                ((PotionRetentionTarget)defender).applyRetainedPotionEffect(effect);
            }
        }
    }
}