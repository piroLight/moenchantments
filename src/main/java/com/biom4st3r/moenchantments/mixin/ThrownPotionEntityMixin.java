package com.biom4st3r.moenchantments.mixin;

import java.util.Iterator;
import java.util.List;

import com.biom4st3r.biow0rks.Biow0rks;
import com.biom4st3r.moenchantments.interfaces.PotionEffectRetainer;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.thrown.ThrownPotionEntity;
import net.minecraft.util.math.Box;


@Mixin(ThrownPotionEntity.class)
public abstract class ThrownPotionEntityMixin
{
    @SuppressWarnings("rawtypes")
    @Inject(
        at=@At(
            value = "INVOKE_ASSIGN", 
            target="Lnet/minecraft/entity/LivingEntity;addPotionEffect(Lnet/minecraft/entity/effect/StatusEffectInstance;)Z",
            shift = Shift.BEFORE
            ),
        method="applySplashPotion",
        locals = LocalCapture.CAPTURE_FAILHARD)
    //@ModifyVariable(at=@At(value = "INVOKE_ASSIGN", target="addPotionEffect",shift = Shift.AFTER), method = "method_7498")
    private void applySplashPotion(List<StatusEffectInstance> list_1, Entity entity_1,CallbackInfo ci, Box bb, List l, Iterator i1,LivingEntity livingEntity_1, double double1,double double2,Iterator i2,StatusEffectInstance sei,StatusEffect se,int i)
    {
        StatusEffectInstance statuseffect = list_1.get(0);
        LivingEntity livingEntity;// = (LivingEntity) (Object) entity_1;
        PotionEffectRetainer mainhand;// = ((PotionEffectRetainer) (Object) livingEntity.getMainHandStack());
        PotionEffectRetainer offhand;// = ((PotionEffectRetainer) (Object) livingEntity.getOffHandStack());
        PotionEffectRetainer retainer;// = mainhand.isPotionRetainer() > 0 ? mainhand : offhand.isPotionRetainer() > 0 ? offhand : null;
        
        if(entity_1 != null && !entity_1.world.isClient() && entity_1 instanceof LivingEntity)
        {
            livingEntity = (LivingEntity)entity_1;
            mainhand = ((PotionEffectRetainer) (Object) livingEntity.getMainHandStack());
            offhand = ((PotionEffectRetainer) (Object) livingEntity.getOffHandStack());
            retainer = mainhand.isPotionRetainer() > 0 ? mainhand : offhand.isPotionRetainer() > 0 ? offhand : null;
            Biow0rks.debug(statuseffect.getEffectType().getTranslationKey());
            if ((statuseffect != null && retainer != null) && ((retainer.getCharges() <= 0 ) || (statuseffect.getEffectType() == retainer.getEffect() && statuseffect.getAmplifier() == retainer.getAmplification()))) {
                int tokens = (int)Math.ceil((statuseffect.getDuration() / 20)/(10));
                retainer.setPotionEffectAndCharges(statuseffect, tokens);
            }
        }
        else if(livingEntity_1 != null)
        {
            //
            mainhand = ((PotionEffectRetainer) (Object) livingEntity_1.getMainHandStack());
            offhand = ((PotionEffectRetainer) (Object) livingEntity_1.getOffHandStack());
            retainer = mainhand.isPotionRetainer() > 0 ? mainhand : offhand.isPotionRetainer() > 0 ? offhand : null;
            if(retainer != null && ((retainer.getCharges() <= 0 ) || (statuseffect.getEffectType() == retainer.getEffect() && statuseffect.getAmplifier() == retainer.getAmplification())))
            {
                //int distanceModifyDuration = (int)(double2 * (double)statuseffect.getDuration() + 0.5D);
                int tokens = (int)Math.ceil((i / 20)/(10));
                retainer.setPotionEffectAndCharges(new StatusEffectInstance(se,0,statuseffect.getAmplifier()), tokens);
            }

            //((PotionRetentionTarget) livingEntity_1).applyRetainedPotionEffect(new StatusEffectInstance(statuseffect.getEffectType(),distanceModifyDuration,statuseffect.getAmplifier()));
        }
    }
}