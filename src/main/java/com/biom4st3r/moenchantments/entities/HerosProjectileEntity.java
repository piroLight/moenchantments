// package com.biom4st3r.moenchantments.entities;

// import com.biom4st3r.biow0rks.Biow0rks;
// import com.biom4st3r.biow0rks.entities.EntityPacket;
// import com.biom4st3r.moenchantments.MoEnchantmentsMod;
// import com.biom4st3r.moenchantments.MoEnchants;

// import net.fabricmc.api.EnvType;
// import net.fabricmc.api.Environment;
// import net.minecraft.enchantment.EnchantmentHelper;
// import net.minecraft.enchantment.Enchantments;
// import net.minecraft.entity.Entity;
// import net.minecraft.entity.LivingEntity;
// import net.minecraft.entity.damage.DamageSource;
// import net.minecraft.entity.damage.ProjectileDamageSource;
// import net.minecraft.entity.projectile.AbstractFireballEntity;
// import net.minecraft.item.ItemStack;
// import net.minecraft.item.Items;
// import net.minecraft.network.Packet;
// import net.minecraft.particle.ParticleEffect;
// import net.minecraft.particle.ParticleTypes;
// import net.minecraft.util.Identifier;
// import net.minecraft.util.hit.EntityHitResult;
// import net.minecraft.util.hit.HitResult;
// import net.minecraft.util.hit.HitResult.Type;
// import net.minecraft.util.math.MathHelper;
// import net.minecraft.world.World;

// public class HerosProjectileEntity extends AbstractFireballEntity {
//     private ItemStack sourceWeapon;

//     public HerosProjectileEntity(World w) {
//         super(MoEnchantmentsMod.herosProjectile, w);
//         this.setItem(new ItemStack(Items.FIREWORK_STAR,1));
//     }

//     public HerosProjectileEntity(World w, LivingEntity sender, double x, double y, double z) {
//         super(MoEnchantmentsMod.herosProjectile, sender, x, y, z, w);
//         if (EnchantmentHelper.getLevel(MoEnchants.HEROSSWORD, sender.getMainHandStack()) > 0) {
//             sourceWeapon = sender.getMainHandStack();
//         }
//         this.setItem(new ItemStack(Items.FIREWORK_STAR,1));
//         System.out.println("initilized");
//         this.init(sender);
//     }

//     public void init(LivingEntity e)
//     {
//         this.owner = e;
//         this.setPositionAndAngles(e.x, e.y+e.getStandingEyeHeight(), e.z, e.yaw, e.pitch);
//         //this.y += e.getStandingEyeHeight();
//         //pi/180
//         float radToDegree = 0.0174532925199F;//0.01745329251994329576923690768489‬
//         float xVelo = -MathHelper.sin(e.yaw * radToDegree) * MathHelper.cos(e.pitch * radToDegree);
//         float yVelo = -MathHelper.sin((e.pitch) * radToDegree);
//         float zVelo = MathHelper.cos(e.yaw * radToDegree) * MathHelper.cos(e.pitch * radToDegree);
//         this.setVelocity(xVelo, yVelo, zVelo);
//     }

//     @Override
//     public void tick() {
//         System.out.println(String.format("%.02f, %.02f, %.02f", this.x, this.y, this.z));
        
//         if (this.owner != null && this.distanceTo(this.owner) > 8 * 10) {
//             this.remove();
//         } else if (this.owner == null && !this.world.isClient) {
//             this.remove();
//         }
//         super.tick();
//     }

//     @Override
//     protected boolean isBurning() {
//         // TODO Auto-generated method stub
//         return false;
//     }

    

//     @Override
//     protected void onCollision(HitResult hit) 
//     {
//         if (!this.world.isClient)
//         {
//             if (hit.getType() == Type.ENTITY)
//             {
//                 EntityHitResult ehit = (EntityHitResult) hit;
//                 if(!(ehit.getEntity() instanceof LivingEntity) || ehit.getEntity() == this.owner)
//                 {
//                     return;
//                 }
//                 LivingEntity defender = (LivingEntity) ehit.getEntity();
//                 defender.damage(HeroProjectileDamageSource.create(this.owner, defender, sourceWeapon), this.sourceWeapon.getDamage() / 2);
//                 float knockback = EnchantmentHelper.getLevel(Enchantments.KNOCKBACK, sourceWeapon) * 0.4f;// vanilla for knockback is 0.5f
//                 defender.takeKnockback(
//                     this.owner, 
//                     (knockback > 0 ? knockback : 0.4f), 
//                     (double) MathHelper.sin(this.yaw * 0.017453292F),
//                     (double) (-MathHelper.cos(this.yaw * 0.017453292F))
//                     );
//             }
//             if (hit.getType() == Type.BLOCK) 
//             {
//                 this.remove();
//             }
//         }
//     }

//     @Override
//     public void remove() {
//         Biow0rks.debug("%s\n%s\n", Thread.currentThread().getStackTrace()[1],Thread.currentThread().getStackTrace()[2]);
//         super.remove();
//     }

//     @Override
//     protected ItemStack getItem() 
//     {
//         return new ItemStack(MoEnchantmentsMod.dhpi, 1);
//     }

//     @Override
//     protected float getDrag() {
//         return 1.0f;
//     }

//     @Override
//     @Environment(EnvType.CLIENT)
//     public ItemStack getStack() 
//     {
//         return new ItemStack(MoEnchantmentsMod.dhpi, 1);
//     }

//     @Override
//     protected ParticleEffect getParticleType() 
//     {
//         return ParticleTypes.SMOKE;
//     }

//     public static Identifier packetID = new Identifier(MoEnchantmentsMod.MODID,"heroprojspawn");
    
//     @Override
//     public Packet<?> createSpawnPacket() {
//         Biow0rks.debug("HeroProjectile.createSpawnPacket", "Success");
//         return EntityPacket.createSpawnPacket(this, packetID);  
//         // PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
//         // pbb.writeVarInt(Registry.ENTITY_TYPE.getRawId(MoEnchantmentsMod.herosProjectile));
//         // pbb.writeUuid(this.uuid);
//         // pbb.writeVarInt(this.getEntityId());
//         // pbb.writeDouble(this.x);
//         // pbb.writeDouble(this.y);
//         // pbb.writeDouble(this.z);
//         // pbb.writeByte(MathHelper.floor(this.pitch * 256.0F / 360.0F));
//         // pbb.writeByte(MathHelper.floor(this.yaw * 256.0F / 360.0F));
//         // return ServerSidePacketRegistry.INSTANCE.toPacket(packetID, pbb);
//         // //return new CustomPayloadS2CPacket(packetID, pbb);
//     }
// }

// class HeroProjectileDamageSource extends ProjectileDamageSource
// {
//     public HeroProjectileDamageSource(Entity attacker, Entity defender, ItemStack iS)
//     {
//         super("heroslice",attacker,defender);
//         this.setBypassesArmor();
//         if(EnchantmentHelper.getLevel(Enchantments.FIRE_ASPECT, iS) > 0)
//         {
//             this.setFire();
//         }
//     }

//     public static DamageSource create(Entity attacker, Entity defender,ItemStack iS)
//     {
//         return new HeroProjectileDamageSource(attacker, defender, iS);
//     }
// }