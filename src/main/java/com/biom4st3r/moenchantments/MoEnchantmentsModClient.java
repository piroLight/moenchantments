package com.biom4st3r.moenchantments;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.ClientModInitializer;
import net.minecraft.server.network.packet.CustomPayloadC2SPacket;
import net.minecraft.util.PacketByteBuf;

public class MoEnchantmentsModClient implements ClientModInitializer {

    @Override
    public void onInitializeClient() {

        // EntityRendererRegistry.INSTANCE.register(HerosProjectileEntity.class, (dispatcher,context) -> 
        // {
        //     return new FlyingItemEntityRenderer<HerosProjectileEntity>(dispatcher, context.getItemRenderer());
        // });
        // EntityPacket.client_RegisterEntityPacket(HerosProjectileEntity.packetID);
    }


    public static CustomPayloadC2SPacket createMissPacket()
	{
		PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
		return new CustomPayloadC2SPacket(MoEnchantmentsMod. MISSEDATTACK,pbb);
	}
}