package com.biom4st3r.moenchantments.Logic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import com.biom4st3r.biow0rks.Biow0rks;
import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CropBlock;
import net.minecraft.block.LogBlock;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public final class onBreakMixinLogic
{

    public static int cap(int val, int ceiling)
	{
		return val > ceiling ? ceiling : val;
	}

	public static void tryVeinMining(World world,BlockPos blockPos,BlockState blockState,PlayerEntity pe)
    {
		if(world.isClient)
		{
			return;
		}
		ItemStack tool = pe.getMainHandStack();
		if(tool == ItemStack.EMPTY || !tool.hasEnchantments())
		{
			return;
		}
		boolean hasAutoSmelt = false;

		int VeinMinerLvl = EnchantmentHelper.getLevel(MoEnchants.VEINMINER, tool);
		int TreeFellerLvl = EnchantmentHelper.getLevel(MoEnchants.TREEFELLER,tool);
		//int havesterLvl = EnchantmentHelper.getLevel(MoEnchants.HAVESTER,tool);
		hasAutoSmelt = EnchantmentHelper.getLevel(MoEnchants.AUTOSMELT, tool) > 0;

		Block currentType = blockState.getBlock();
		int brokenBlocks = 0;

		if(TreeFellerLvl > 0)
        {
            if((currentType instanceof LogBlock))
            {
				brokenBlocks = doVeinMiner(blockState,world,blockPos,MoEnchantmentsMod.config.TreeFellerMaxBreakByLvl[TreeFellerLvl-1],pe,tool);
				Biow0rks.debug( "%s block broken",brokenBlocks);
			}
		}
		else if(VeinMinerLvl > 0)
		{
			if(isValidBlockForPick(currentType))
			{
				brokenBlocks = doVeinMiner(blockState,world,blockPos,MoEnchantmentsMod.config.VeinMinerMaxBreakByLvl[VeinMinerLvl-1],pe,tool);
				Biow0rks.debug( "%s block broken",brokenBlocks);
			}
		}
		pe.increaseStat(Stats.MINED.getOrCreateStat(currentType), brokenBlocks);
		if(hasAutoSmelt)
		{
			int retrievedXp = ((PlayerExperienceStore)(Object)pe).getAndRemoveStoredExp();
			Biow0rks.debug("retrieved XP %s", retrievedXp);
			for(; retrievedXp > 0; retrievedXp--)
			{
				dropExperience(1, blockPos, world);
			}
			// if(tool.getTag().containsKey("smeltexperience"))
			// {

			// 	double exp = tool.getTag().getDouble("smeltexperience");
			// 	if(exp > 1)
			// 	{
			// 		tool.getTag().putDouble("smeltexperience", 0);
			// 		for(;exp > 1; exp--)
			// 		{
			// 			dropExperience(1f, blockPos, world);
			// 		}
			// 	}
			// }
		}
	}

	public static ArrayList<CropBlock> getCropBlocks(World w, BlockPos source, Block type)
	{
		return null;
	}
	
    public static ArrayList<BlockPos> getSameBlocks(World w,BlockPos source, Block type)
    {
        ArrayList<BlockPos> t = new ArrayList<BlockPos>();
		for(BlockPos pos : new BlockPos[] {source.up().south(),source.up().east(),source.up().west(),source.up().north(),
											source.up(),source.down(),source.north(),source.east(),source.south(),source.west(),
											source.down().north(),source.down().east(),source.down().west(),source.down().south()})
        {
            if(w.getBlockState(pos).getBlock() == type)
            {
                t.add(pos);
            }
		}
		
        return t;
	}

	private static boolean isValidBlockForPick(Block block)
	{
		if(block == Blocks.STONE || block == Blocks.ANDESITE || block == Blocks.GRANITE || block == Blocks.DIORITE)
		{

			return false;
		}
		// if(block instanceof OreBlock)
		// {
		// 	return true;
		// }
		// else if(block instanceof RedstoneOreBlock)
		// {
		// 	return true;
		// }

		// else if(MoEnchantmentsMod.class_whitelist.contains(block.getClass()))
		// {
		// 	util.Debug("onBreakMixinLogin#isValiVeiningBlock; class_whitelist", block.toString() + " true");
		// 	return true;
		// }
		
		else if(MoEnchantmentsMod.block_whitelist.contains(block))
		{
			Biow0rks.debug("%s block_whitelist", block.toString());
			return true;
		}
		Biow0rks.debug("%s global false",block.toString());
		return false;
	}
	
	private static void dropExperience(float experienceValue ,BlockPos blockPos,World world)
	{
		world.spawnEntity(new ExperienceOrbEntity(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), (int)experienceValue));
	}

	private static int doVeinMiner(BlockState blockState,World world,BlockPos blockPos,int maxBlocks,PlayerEntity pe,ItemStack tool)
	{
		Biow0rks.debug("BlockType %s", blockState.getBlock());
		//ItemStack recipeResult = recipe.isPresent() ? recipe.get().getOutput().copy() : ItemStack.EMPTY;
		int blocksBroken = 0;
		//float experiencetotal = 0.0f;
		Queue<BlockPos> toBreak = new LinkedList<BlockPos>();
		toBreak.addAll(getSameBlocks(world,blockPos,blockState.getBlock()));
		System.out.println(String.format("Max Dam: %s\nCur Dam: %s\n %s", tool.getMaxDamage(),tool.getDamage(),tool.getMaxDamage()-tool.getDamage()));
		while(!toBreak.isEmpty() && blocksBroken <= (maxBlocks))
		{
			Biow0rks.debug("%s Blocks to break" ,toBreak.size());
			Biow0rks.debug("blocksBroken out of max: %s/%s", blocksBroken, maxBlocks);
			BlockPos currPos = toBreak.remove();
			//double distance = pe.getBlockPos().getSquaredDistance(new Vec3i(currPos.getX(),currPos.getY(),currPos.getZ()));
			
			if(toBreak.size() < 50)
				toBreak.addAll(getSameBlocks(world, currPos, blockState.getBlock()));

			int maxDistance = MoEnchantmentsMod.config.MaxDistanceFromPlayer;
			if(Math.abs(currPos.getX() - pe.x) < maxDistance && Math.abs(currPos.getZ() - pe.z) < maxDistance && world.getBlockState(currPos).getBlock() == blockState.getBlock())
			{
				Biow0rks.debug("valid Block");
				Block.dropStacks(world.getBlockState(currPos), world, pe.getBlockPos(), null, pe, tool);
				world.breakBlock(currPos, false);
				
				tool.damage(1, pe,(playerEntity_1) -> {
					playerEntity_1.sendToolBreakStatus(pe.getActiveHand());
				});
				if((MoEnchantmentsMod.config.ProtectItemFromBreaking ? tool.getMaxDamage()-tool.getDamage() < 10 : true))
				{
					pe.playSound(SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.PLAYERS, 1f, 1f);
					pe.sendMessage(new TranslatableText("My tool is badly damaged").formatted(Formatting.GREEN));
					break;
				}
				blocksBroken++;
			}
			world.setBlockState(currPos, Blocks.AIR.getDefaultState());
		}
		pe.addExhaustion(0.005F * blocksBroken);
		return blocksBroken;
	}
}